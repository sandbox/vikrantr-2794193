This is useful module to get bulk Latitude & Longitude from respective address in xls.

Steps:
1) Download & enable this module
2) Create new .xls sheet with all set of addresses in placed, Just need to add addresses from first row & first columns only, no other column required.
3) go to path "get_lat_long_by_address"
4) Browse the address file.
5) It will get process and new file will get download adding lat & long for each row.
